const { expect } = require("chai");
const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);

describe("get requests", () => {
  it("should be 200", (done) => {
    chai
      .request("https://httpbin.org")
      .get("/get")
      .end(function (err, res) {
        expect(res).to.have.status(200);
        done();
      });
  });
  it("should not have error", (done) => {
    chai
      .request("https://httpbin.org")
      .get("/get")
      .end(function (err, res) {
        expect(err).to.be.null;
        done();
      });
  });
  it("should be 500", (done) => {
    chai
      .request("https://httpbin.org")
      .get("/status/500")
      .end(function (err, res) {
        expect(res).to.have.status(500);
        done();
      });
  });
});
